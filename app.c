/***************************************************************************/ /**
* @file
* @brief Core application logic.
*******************************************************************************
* # License
* <b>Copyright 2022 Silicon Laboratories Inc. www.silabs.com</b>
*******************************************************************************
*
* SPDX-License-Identifier: Zlib
*
* The licensor of this software is Silicon Laboratories Inc.
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*
******************************************************************************/
#include <stdio.h>
#include "em_common.h"
#include "app_assert.h"
#include "sl_status.h"
#include "app.h"
#include "gatt_db.h"

#include "sl_btmesh_api.h"
#include "sl_bt_api.h"
#include "sl_btmesh_generic_model_capi_types.h"
#include "sl_btmesh_lib.h"

#include "app_button_press.h"
#include "sl_simple_button_instances.h"
#include "sl_btmesh_factory_reset.h"
#include "sl_simple_timer.h"
#include "sl_btmesh_dcd.h"

/// Advertising Provisioning Bearer
#define PB_ADV 0x1
/// GATT Provisioning Bearer
#define PB_GATT 0x2

#define GENERIC_ON_OFF_MODEL_ID 0x1000
#define GENERIC_LEVEL_MODEL_ID 0x1002
#define ELEMENT_INDEX 0
#define RESPONSE_REQUIRED_FLAG 0x10

/// Length of the display name buffer
#define NAME_BUF_LEN 20

static bd_addr bdAddress;

static void register_callbacks(void);
static void handle_boot_event(void);
static bool handle_reset_conditions(void);
static void set_device_name(bd_addr* addr);
/**************************************************************************/ /**
* Application Init.
*****************************************************************************/
SL_WEAK void app_init(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application init code here!                         //
  // This is called once during start-up.                                    //
  /////////////////////////////////////////////////////////////////////////////
  app_button_press_enable();
}

/**************************************************************************/ /**
* Application Process Action.
*****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
  if (sl_simple_button_get_state(&sl_button_btn0) == SL_SIMPLE_BUTTON_PRESSED) {
      app_button_press_disable();
      app_log("full factory reset\r\n");
      // Full factory reset
      sl_btmesh_initiate_full_reset();
  }
}

/**************************************************************************/ /**
* Bluetooth stack event handler.
* This overrides the dummy weak implementation.
*
* @param[in] evt Event coming from the Bluetooth stack.
*****************************************************************************/
void sl_bt_on_event(struct sl_bt_msg *evt)
{
  sl_status_t sc;
  switch (SL_BT_MSG_ID(evt->header))
  {
  case sl_bt_evt_system_boot_id:
    // Initialize Mesh stack in Node operation mode,
    // wait for initialized event
    app_log("Node init\r\n");

#if 1 // set to 0 to clear provisioning data
    //sc = sl_btmesh_node_init();
    //app_assert_status_f(sc, "Failed to init node\n");
    handle_boot_event();
#else
    // clear provisioning data
    sl_btmesh_node_reset();
#endif
    break;
  ///////////////////////////////////////////////////////////////////////////
  // Add additional event handlers here as your application requires!      //
  ///////////////////////////////////////////////////////////////////////////

  // -------------------------------
  // Default event handler.
  default:
    break;
  }
}

/**************************************************************************/ /**
* Bluetooth Mesh stack event handler.
* This overrides the dummy weak implementation.
*
* @param[in] evt Event coming from the Bluetooth Mesh stack.
*****************************************************************************/
void sl_btmesh_on_event(sl_btmesh_msg_t *evt)
{
  sl_status_t sc;
  switch (SL_BT_MSG_ID(evt->header))
  {
  case sl_btmesh_evt_node_initialized_id:
    if (!evt->data.evt_node_initialized.provisioned)
    {
      app_log("Not provisioned, starting unprovisioned beaconing\r\n");
      // The Node is now initialized,
      // start unprovisioned Beaconing using PB-ADV and PB-GATT Bearers
      sc = sl_btmesh_node_start_unprov_beaconing(PB_ADV | PB_GATT);
      app_assert_status_f(sc, "Failed to start unprovisioned beaconing\n");
    }
    else
    {
      app_log("Already provisioned\r\n");
      register_callbacks();
    }
    break;

  ///////////////////////////////////////////////////////////////////////////
  // Add additional event handlers here as your application requires!      //
  ///////////////////////////////////////////////////////////////////////////

  // -------------------------------
  // Default event handler.
  default:
    break;
  }
}

static void client_request_cb(uint16_t model_id,
                              uint16_t element_index,
                              uint16_t client_addr,
                              uint16_t server_addr,
                              uint16_t appkey_index,
                              const struct mesh_generic_request *req,
                              uint32_t transition_ms,
                              uint16_t delay_ms,
                              uint8_t request_flags)
{

  sl_status_t sc;

  if (model_id == GENERIC_ON_OFF_MODEL_ID)
  {
    struct mesh_generic_state state;
    state.kind = mesh_generic_state_on_off;
    state.on_off.on = req->on_off;
    if ((request_flags & RESPONSE_REQUIRED_FLAG) == RESPONSE_REQUIRED_FLAG)
    {
      sc = mesh_lib_generic_server_respond(model_id, element_index, client_addr, appkey_index, &state, NULL, 0, request_flags);
      app_assert_status_f(sc, "Failed to send client response [ON-Off]\n");
    }
    else
    {
      sc = mesh_lib_generic_server_update(model_id, element_index, &state, NULL, 0);
      app_assert_status_f(sc, "Failed to server update [ON-OFF]\n");
    }
  }
  else if (model_id == GENERIC_LEVEL_MODEL_ID)
  {
    struct mesh_generic_state state;
    state.kind = mesh_generic_state_level;
    state.level.level = req->level;
    if ((request_flags & RESPONSE_REQUIRED_FLAG) == RESPONSE_REQUIRED_FLAG)
    {
      sc = mesh_lib_generic_server_respond(model_id, element_index, client_addr, appkey_index, &state, NULL, 0, request_flags);
      app_assert_status_f(sc, "Failed to send client response [LEVEL]\n");
    }
    else
    {
      sc = mesh_lib_generic_server_update(model_id, element_index, &state, NULL, 0);
      app_assert_status_f(sc, "Failed to server update [LEVEL]\n");
    }

  }
}

#if 0
static void server_change_cb(uint16_t model_id,
                             uint16_t element_index,
                             const struct mesh_generic_state *current,
                             const struct mesh_generic_state *target,
                             uint32_t remaining_m)
{
}

static void server_recall_cb(uint16_t model_id,
                             uint16_t element_index,
                             const struct mesh_generic_state *current,
                             const struct mesh_generic_state *target,
                             uint32_t transition_time_ms)
{
}
#endif

static void register_callbacks(void)
{
  sl_status_t sc;
  sc = mesh_lib_generic_server_register_handler(GENERIC_ON_OFF_MODEL_ID, ELEMENT_INDEX, client_request_cb, NULL, NULL);
  app_assert_status_f(sc, "Failed register handler [ON-OFF]\n");
  sc = mesh_lib_generic_server_register_handler(GENERIC_LEVEL_MODEL_ID, ELEMENT_INDEX, client_request_cb, NULL, NULL);
  app_assert_status_f(sc, "Failed register handler [LEVEL]\n");
}

static void handle_boot_event(void) {
  sl_status_t sc;
  uint8_t address_type;
  // Check reset conditions and continue if not reset.
  if (handle_reset_conditions()) {
    sc = sl_bt_system_get_identity_address(&bdAddress, &address_type);
    app_assert_status_f(sc, "Failed to get Bluetooth address\r\n");
    set_device_name(&bdAddress);
    // Initialize Mesh stack in Node operation mode, wait for initialized event
    sc = sl_btmesh_node_init();
    if (sc != SL_STATUS_OK) {
      app_log("[E: 0x%04x] sl_btmesh_node_init() failed\r\n", (int)sc);
    }
  }
}

static bool handle_reset_conditions(void) {
  // If PB0 is held down then do full factory reset.
  /* this is currently not working, the button press is detected but the
   * factory reset process does not occur.  The debug print statement shows a
   * single not alphabetical character.
   */
  if (sl_simple_button_get_state(&sl_button_btn0) == SL_SIMPLE_BUTTON_PRESSED) {
    // Disable button presses
    app_button_press_disable();
    app_log("full factory reset\r\n");
    // Full factory reset
    sl_btmesh_initiate_full_reset();
    return false;
  }
  return true;
}

static void set_device_name(bd_addr* addr) {
  char name[NAME_BUF_LEN];
  sl_status_t result;

  // Create unique device name using the last two bytes of the Bluetooth address
  snprintf(name, NAME_BUF_LEN, "soc server: %02x:%02x", addr->addr[1], addr->addr[0]);

  app_log("Device name: '%s'\r\n", name);

  result = sl_bt_gatt_server_write_attribute_value(gattdb_device_name, 0, strlen(name), (uint8_t*)name);
  if (result != SL_STATUS_OK) {
    app_log("sl_bt_gatt_server_write_attribute_value() failed, code %x\r\n", result);
  }
}
